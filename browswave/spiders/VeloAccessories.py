import scrapy
import json
import re
from scrapy.http import FormRequest

from browswave.items import VeloAccItem


class VeloAccessoriesSpider(scrapy.Spider):
    name = 'veloacc'
    allowed_domains = ['mr-bricolage.bg']
    start_urls = ['https://mr-bricolage.bg/instrumenti/velosipedi-i-chasti/veloaksesoari/c/006016001']

    def parse(self, response):
    
        for product in response.css('div.product div.image'):
            product_link = product.css('a::attr(href)').get();
            yield response.follow(product_link, callback=self.parse_product)
            
        next_page = response.css('li.pagination-next a::attr(href)').get()
        
        if next_page is not None:
            yield response.follow(next_page, self.parse)    
        
    def parse_product(self, response):
        
        def extract_with_css(query):
            return response.css(query).get(default='').strip()
        
        item = VeloAccItem() 
               
        item['name'] = extract_with_css('h1.js-product-name::text'),
        item['price'] = extract_with_css('p.js-product-price::attr(data-price-value)'),
        item['image'] = extract_with_css('a.test-popup-link::attr(href)'),
        item['productcode'] = re.search(r'(\d+$)', extract_with_css('div.bricolage-code::text')).group(), 
        
        product_characteristics = {}
        for characteristic in response.css('div.product-classifications tr'):
            attr = characteristic.css('td.attrib::text').get()
            characteristic_text = characteristic.css('td.attrib + td::text').get()
            product_characteristics[attr] = " ".join( characteristic_text.split() )
        
        item['characteristics'] = product_characteristics
        
        # availability in shops
        
        form_command = response.css('form#command')
        form_action = form_command.css('form::attr(action)').get()
        locationQuery = '' #form_command.css('input[name=locationQuery]::attr(value)').get()
        cartPage = form_command.css('input[name=cartPage]::attr(value)').get()
        entryNumber = form_command.css('input[name=entryNumber]::attr(value)').get()
        CSRFToken = form_command.css('input[name=CSRFToken]::attr(value)').get()
        
        locationQuery = '' #form_command.css('input[name=locationQuery]::attr(value)').get()
        cartPage = 'false' #form_command.css('input[name=cartPage]::attr(value)').get()
        entryNumber = '0' #form_command.css('input[name=entryNumber]::attr(value)').get()
        CSRFToken = form_command.css('input[name=CSRFToken]::attr(value)').get()
        
        url = 'https://mr-bricolage.bg' + form_action
        data = {
            'locationQuery': locationQuery,
            'cartPage': cartPage,
            'entryNumber': entryNumber,
            'latitude':	'0', #"42.6641056",
            'longitude': '0', #"23.3233149",
            'CSRFToken': CSRFToken
        }
        
        yield scrapy.http.FormRequest(url, callback=self.parse_availability, formdata=data,  method='POST', meta={'item':item})

        
    def parse_availability(self, response):
        jsonresponse = json.loads(response.text)
        
        item = response.meta['item']
        item['availability'] = jsonresponse['data']
        
        yield item
        
