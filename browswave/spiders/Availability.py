import scrapy
import json
import re
from scrapy.http import FormRequest
      

class AvailabilitySpider(scrapy.Spider):
    name = 'availability'
    allowed_domains = ['mr-bricolage.bg']
    start_urls = ['https://mr-bricolage.bg/instrumenti/velosipedi-i-chasti/veloaksesoari/c/006016001']

    def parse(self, response):
    
        for product in response.css('div.product div.image'):
            product_link = product.css('a::attr(href)').get();
            yield response.follow(product_link, callback=self.parse_product)
            
        next_page = response.css('li.pagination-next a::attr(href)').get()
        
        if next_page is not None:
            yield response.follow(next_page, self.parse)    
        
    def parse_product(self, response):
        
        form_command = response.css('form#command')
        form_action = form_command.css('form::attr(action)').get()
        locationQuery = '' #form_command.css('input[name=locationQuery]::attr(value)').get()
        cartPage = form_command.css('input[name=cartPage]::attr(value)').get()
        entryNumber = form_command.css('input[name=entryNumber]::attr(value)').get()
        CSRFToken = form_command.css('input[name=CSRFToken]::attr(value)').get()
        
        locationQuery = '' #form_command.css('input[name=locationQuery]::attr(value)').get()
        cartPage = 'false' #form_command.css('input[name=cartPage]::attr(value)').get()
        entryNumber = '0' #form_command.css('input[name=entryNumber]::attr(value)').get()
        CSRFToken = form_command.css('input[name=CSRFToken]::attr(value)').get()
        
        url = 'https://mr-bricolage.bg' + form_action
        data = {
            'locationQuery': locationQuery,
            'cartPage': cartPage,
            'entryNumber': entryNumber,
            'latitude':	'0', #"42.6641056",
            'longitude': '0', #"23.3233149",
            'CSRFToken': CSRFToken
        }
        
        yield scrapy.http.FormRequest(url, callback=self.parse_availability, formdata=data,  method='POST')
        
        
    def parse_availability(self, response):
        jsonresponse = json.loads(response.text)
                    
        yield jsonresponse


       
       